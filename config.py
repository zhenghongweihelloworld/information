import logging
from redis import StrictRedis


class Config(object):
    # 项目的配置
    #DEBUG = True
    SECRET_KEY = 'hDACdLuTNEXZM9e3g2sRf+53hWNlPdAeQtDq1FCIyvOxHwwnHuCfqIvHcd7HOU4R'
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/information27'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    #在请求结束时 如果指定此配置为True，那么SQLAlchemy会自动执行一次db.session.commit()操作
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # redis 的配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = '6379'
    # session保存配置
    SESSION_TYPE = 'redis'
    # 开启session签名
    SESSION_USE_SIGNER = True
    # 指定Session保存的redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 设置需要过期
    SESSION_PERMANENT = False
    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2

    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    #开发环境下的配置
    DEBUG = True


class ProducingConfig(Config):
    #生产环境下的配置
    DEBUG = False
    LOG_LEVEL = logging.WARNING


class TestingConfig(Config):
    DEBUG = True
    TESTING = True



config = {
    'development':DevelopmentConfig,
    'produring':ProducingConfig,
    'testing':TestingConfig
}






