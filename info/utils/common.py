#共用的工具类
import functools

from flask import current_app
from flask import g
from flask import session

from info.models import User


def do_index_class(index):
    if index ==1:
        return "first"
    elif index == 2:
        return "second"
    elif index == 3:
        return "third"
    return ""


def user_login_data(f):
    @functools.wraps(f)
    def wrapper(*args,**kwargs):
        # 取到用户id
        user_id = session.get('user_id', None)
        user = None
        if user_id:
            # 尝试查询用户的模型
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        g.user = user
        return f(*args,**kwargs)
    return wrapper